<?php

class MyPost {
  public function __construct() {
    $home = $_SERVER['HOME'];

    $self_dir = dirname( __FILE__ );

    $line = "alias ost='" . $self_dir . "/script.sh'";

    if ( ! file_exists( $home . '/.bash_profile' ) ) {
      $profile_file = $home . '/.profile';
    }
    else {
      $profile_file = $home . '/.bash_profile';
    }

    $contents = file_get_contents( $profile_file );

    if ( strpos( $contents, $line ) !== true ) {
      file_put_contents( $profile_file, $line . PHP_EOL, FILE_APPEND );
    }

    system( 'chmod 755 ' . $self_dir . "/script.sh" );
    system( 'source ' . $profile_file );
    system( 'ost' );

  }
}

new MyPost();
